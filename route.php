<?php 
require_once('controllers/controller.php');
require_once('views/head.php');
switch ($url) {
	case '/':
		require_once('views/main.php');
		break;
	default:
		require_once('views/404.php');
		break;
}
require_once('views/footer.php');
